/*     */ package somepeople.lobbys.data;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import java.io.File;
/*     */ import java.io.FileNotFoundException;
/*     */ import java.io.IOException;
/*     */ import java.io.Reader;
/*     */ import java.io.Writer;
/*     */ import java.util.TreeMap;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ import somepeople.lobbys.main.Main;
/*     */ 
/*     */ public class Messages
/*     */ {
/*  16 */   private static TreeMap<String, String> config = new TreeMap();
/*     */   
/*     */   private static String message;
/*  19 */   private static final java.lang.reflect.Type TYPE = new com.google.gson.reflect.TypeToken() {}.getType();
/*     */   
/*  21 */   public static final String NO_PEX = ChatColor.RED + "[Lobbys] Увы, у Вас недостаточно прав";
/*     */   
/*     */ 
/*     */   public static final String HELP = "Список команд плагина Lobbys:\n/lobby - телепорт в лобби\n/lobbys reload - перезагрузить плагин\n/tserver %lobby% - телепорт в лобби сервера\n/tserver player %player% %lobby% - телепорт игрока в лобби";
/*     */   
/*     */ 
/*  27 */   public static final String AUTHOR = "Плагин Lobbys. Автор Some. Специально для " + ChatColor.RED + "Memas" + ChatColor.GOLD + "Gold";
/*     */   
/*     */   public Messages()
/*     */   {
/*  31 */     loadMessages();
/*     */   }
/*     */   
/*     */   public String OfflinePlayer(String player) {
/*  35 */     message = (String)config.get("offline-player");
/*  36 */     message = message.replaceAll("%player%", player);
/*  37 */     return message;
/*     */   }
/*     */   
/*     */   public String OfflineServer(String server) {
/*  41 */     message = (String)config.get("offline-server");
/*  42 */     message = message.replaceAll("%server%", server);
/*  43 */     return message;
/*     */   }
/*     */   
/*     */   public String teleportPlayer(String player, String server) {
/*  47 */     message = (String)config.get("teleport-player");
/*  48 */     message = message.replaceAll("%server%", server);
/*  49 */     message = message.replaceAll("%player%", player);
/*  50 */     return message;
/*     */   }
/*     */   
/*     */   public String teleportMyself(String server) {
/*  54 */     message = (String)config.get("teleport-myself");
/*  55 */     message = message.replaceAll("%server%", server);
/*  56 */     return message;
/*     */   }
/*     */   
/*     */   public String teleportNowhere() {
/*  60 */     return (String)config.get("teleport-nowhere");
/*     */   }
/*     */   
/*     */   public String groupNotFound(String group) {
/*  64 */     message = (String)config.get("group-notfound");
/*  65 */     message = message.replaceAll("%group%", group);
/*  66 */     return message;
/*     */   }
/*     */   
/*     */   private static void loadDefaultMessages() {
/*  70 */     config.clear();
/*     */     
/*  72 */     config.put("offline-player", "[Lobbys] Игрок %player% не в сети");
/*  73 */     config.put("offline-server", "[Lobbys] Сервер %server% не в сети");
/*  74 */     config.put("teleport-player", "[Lobbys] Игрок %player% телепортирован  на сервер %server%");
/*  75 */     config.put("teleport-myself", "[Lobbys] Вы телепортированы на сервер %server%");
/*  76 */     config.put("teleport-nowhere", "[Lobbys] Вы уже в главом лобби");
/*  77 */     config.put("group-notfound", "[Lobbys] Группа %group% не найдена");
/*     */     try
/*     */     {
/*  80 */       Writer writer = new java.io.FileWriter(new File("plugins/Lobbys/messagges.json"));
/*  81 */       Main.gson.toJson(config, writer);
/*  82 */       writer.close();
/*     */     } catch (IOException e) {
/*  84 */       Main.logger.warning("Error when try to load default messages");
/*     */     }
/*     */   }
/*     */   
/*     */   private static void loadMessages() {
/*  89 */     File fileconfig = new File("plugins/Lobbys/messagges.json");
/*     */     try {
/*  91 */       fileconfig.getParentFile().mkdirs();
/*  92 */       if (fileconfig.exists()) {
/*  93 */         Reader reader = new java.io.FileReader(fileconfig);
/*  94 */         config = (TreeMap)Main.gson.fromJson(reader, TYPE);
/*  95 */         reader.close();
/*     */       } else {
/*  97 */         loadDefaultMessages();
/*     */       }
/*  99 */       Main.logger.info("Messages loaded");
/*     */     } catch (FileNotFoundException e) {
/* 101 */       Main.logger.warning("Messages not found");
/*     */     } catch (IOException e) {
/* 103 */       e.getMessage();
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\data\Messages.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */