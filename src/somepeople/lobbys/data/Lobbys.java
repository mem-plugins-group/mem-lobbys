/*     */ package somepeople.lobbys.data;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.reflect.TypeToken;
/*     */ import java.io.File;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.Writer;
/*     */ import java.lang.reflect.Type;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.HashMap;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ import net.md_5.bungee.api.config.ServerInfo;
/*     */ import somepeople.lobbys.main.Main;
/*     */ 
/*     */ public class Lobbys
/*     */ {
/*  21 */   private static HashMap<String, ArrayList<String>> list = new HashMap(0);
/*     */   
/*  23 */   public static final Type typeListLobby = new TypeToken() {}.getType();
/*     */   
/*     */   public boolean addGroup(String name) {
/*  26 */     if (list.containsKey(name)) {
/*  27 */       return false;
/*     */     }
/*  29 */     list.put(name, new ArrayList(0));
/*  30 */     return true;
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean addLobby(String group, String lobby)
/*     */   {
/*  36 */     group = group.toLowerCase();
/*  37 */     lobby = lobby.toLowerCase();
/*     */     
/*  39 */     addGroup(group);
/*     */     
/*  41 */     if (((ArrayList)list.get(group)).contains(lobby)) {
/*  42 */       return false;
/*     */     }
/*  44 */     ((ArrayList)list.get(group)).add(lobby);
/*  45 */     return true;
/*     */   }
/*     */   
/*     */   public void setDefaultLobbyList()
/*     */   {
/*  50 */     list.clear();
/*     */     
/*  52 */     addLobby("Group-1", "Lobby-1");
/*  53 */     addLobby("group-1", "Lobby-2");
/*  54 */     addLobby("group-1", "Lobby-3");
/*     */     
/*  56 */     addLobby("group-2", "Lobby-4");
/*  57 */     addLobby("group-2", "Lobby-5");
/*  58 */     addLobby("group-2", "Lobby-6");
/*     */     
/*  60 */     addLobby("group-3", "Lobby-7");
/*  61 */     addLobby("group-3", "Lobby-8");
/*  62 */     addLobby("group-3", "Lobby-9");
/*     */     
/*  64 */     writeLobbyList();
/*     */   }
/*     */   
/*     */   public void loadLobbyList() {
/*     */     try {
/*  69 */       Main.file_config.getParentFile().mkdirs();
/*     */       
/*  71 */       if (!Main.file_config.createNewFile()) {
/*  72 */         readLobbyList();
/*     */       } else {
/*  74 */         setDefaultLobbyList();
/*     */       }
/*  76 */       Main.logger.info("Config loaded");
/*     */     } catch (IOException e) {
/*  78 */       e.getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void writeLobbyList() {
/*     */     try {
/*  84 */       Main.writer = new FileWriter(Main.file_config);
/*  85 */       Main.gson.toJson(list, Main.writer);
/*  86 */       Main.writer.close();
/*     */     } catch (IOException e) {
/*  88 */       e.getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   public static void readLobbyList() {
/*     */     try {
/*  94 */       Main.reader = new java.io.FileReader(Main.file_config);
/*  95 */       list = (HashMap)Main.gson.fromJson(Main.reader, typeListLobby);
/*  96 */       Main.reader.close();
/*     */       
/*  98 */       if (list == null) list = new HashMap(0);
/*     */     } catch (IOException e) {
/* 100 */       e.getMessage();
/*     */     }
/*     */   }
/*     */   
/*     */   public ArrayList<String> getGroup(String group) {
/* 105 */     group = group.toLowerCase();
/*     */     
/* 107 */     if (list.containsKey(group)) return (ArrayList)list.get(group);
/* 108 */     return null;
/*     */   }
/*     */   
/*     */   public String findGroup(String group)
/*     */   {
/* 113 */     group = group.toLowerCase();
/*     */     
/* 115 */     for (Map.Entry<String, ArrayList<String>> g : list.entrySet()) {
/* 116 */       if (((String)g.getKey()).startsWith(group)) {
/* 117 */         return (String)g.getKey();
/*     */       }
/*     */     }
/* 120 */     return "";
/*     */   }
/*     */   
/*     */   public ServerInfo MinOnlineServer(String server) {
/* 124 */     server = server.toLowerCase();
/*     */     
/* 126 */     ArrayList<String> lobbys = getGroup(server);
/* 127 */     if (lobbys == null) { return null;
/*     */     }
/* 129 */     ServerInfo target = ProxyServer.getInstance().getServerInfo((String)lobbys.get(0));
/* 130 */     if (target == null) { return null;
/*     */     }
/* 132 */     int min = target.getPlayers().size();
/*     */     
/* 134 */     for (int i = 1; i < lobbys.size(); i++) {
/* 135 */       ServerInfo t = ProxyServer.getInstance().getServerInfo((String)lobbys.get(i));
/* 136 */       if (t != null) {
/* 137 */         int online = t.getPlayers().size();
/* 138 */         if (min < online) {
/* 139 */           min = online;
/* 140 */           target = t;
/*     */         }
/*     */       }
/*     */     }
/* 144 */     return target;
/*     */   }
/*     */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\data\Lobbys.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */