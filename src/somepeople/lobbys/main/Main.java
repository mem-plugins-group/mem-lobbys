/*    */ package somepeople.lobbys.main;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.google.gson.GsonBuilder;
/*    */ import java.io.File;
/*    */ import java.io.Reader;
/*    */ import java.io.Writer;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Plugin;
/*    */ import net.md_5.bungee.api.plugin.PluginManager;
/*    */ import somepeople.lobbys.commands.CommandLobby;
/*    */ import somepeople.lobbys.commands.CommandLobbys;
/*    */ import somepeople.lobbys.commands.CommandTServer;
/*    */ import somepeople.lobbys.data.Lobbys;
/*    */ import somepeople.lobbys.data.Messages;
/*    */ import somepeople.lobbys.listeners.Events;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Main
/*    */   extends Plugin
/*    */ {
/*    */   public static Reader reader;
/*    */   public static Writer writer;
/*    */   public static Logger logger;
/*    */   public static Messages msg;
/*    */   public static Plugin plugin;
/* 34 */   public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
/*    */   
/* 36 */   public static Lobbys listLobby = new Lobbys();
/*    */   
/* 38 */   public static File file_config = new File("plugins/Lobbys/config.json");
/*    */   
/*    */   public void onEnable()
/*    */   {
/* 42 */     plugin = this;
/* 43 */     logger = getLogger();
/*    */     
/* 45 */     listLobby.loadLobbyList();
/*    */     
/* 47 */     getProxy().getPluginManager().registerCommand(this, new CommandLobby());
/* 48 */     getProxy().getPluginManager().registerCommand(this, new CommandTServer());
/* 49 */     getProxy().getPluginManager().registerCommand(this, new CommandLobbys());
/* 50 */     getProxy().getPluginManager().registerListener(this, new Events());
/*    */     
/* 52 */     msg = new Messages();
/*    */   }
/*    */   
/*    */   public static void pluginReload(CommandSender sender)
/*    */   {
/* 57 */     if (!sender.hasPermission("lobbys.reload")) {
/* 58 */       sender.sendMessage(new TextComponent(Messages.NO_PEX));
/* 59 */       return;
/*    */     }
/*    */     
/* 62 */     msg = new Messages();
/* 63 */     listLobby.loadLobbyList();
/* 64 */     sender.sendMessage(new TextComponent(ChatColor.GREEN + "[Lobbys] Plugin reloaded"));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\main\Main.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */