/*    */ package somepeople.lobbys.listeners;
/*    */ 
/*    */ import java.util.Map;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.event.ServerConnectEvent;
/*    */ import net.md_5.bungee.api.event.ServerKickEvent;
/*    */ import net.md_5.bungee.api.plugin.Listener;
/*    */ import net.md_5.bungee.event.EventHandler;
/*    */ import somepeople.lobbys.data.Lobbys;
/*    */ import somepeople.lobbys.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Events
/*    */   implements Listener
/*    */ {
/*    */   @EventHandler
/*    */   public void onServerConnect(ServerConnectEvent event)
/*    */   {
/* 22 */     String name = event.getTarget().getName().toLowerCase();
/*    */     
/* 24 */     if (!name.contains("lobby")) { return;
/*    */     }
/* 26 */     ServerInfo target = Main.listLobby.MinOnlineServer(name);
/* 27 */     if ((target != null) && (ProxyServer.getInstance().getServers().containsKey(target.getName()))) {
/* 28 */       event.getPlayer().connect(target);
/*    */     }
/*    */   }
/*    */   
/*    */   @EventHandler
/*    */   public void onServerKickEvent(ServerKickEvent event) {
/* 34 */     String from = event.getKickedFrom().getName().toLowerCase();
/*    */     
/* 36 */     if (from.contains("lobby")) { return;
/*    */     }
/* 38 */     ServerInfo target = Main.listLobby.MinOnlineServer(from);
/* 39 */     if ((target != null) && 
/* 40 */       (ProxyServer.getInstance().getServers().containsKey(target.getName())) && 
/* 41 */       (event.getKickReason().toLowerCase().contains("server closed")))
/*    */     {
/* 43 */       event.setCancelled(true);
/* 44 */       event.setCancelServer(target);
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\listeners\Events.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */