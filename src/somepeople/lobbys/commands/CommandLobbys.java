/*    */ package somepeople.lobbys.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.lobbys.data.Messages;
/*    */ import somepeople.lobbys.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CommandLobbys
/*    */   extends Command
/*    */ {
/*    */   public CommandLobbys()
/*    */   {
/* 16 */     super("lobbys");
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 21 */     switch (args.length) {
/*    */     case 0: 
/* 23 */       sender.sendMessage(new TextComponent(Messages.AUTHOR));
/* 24 */       break;
/*    */     case 1: 
/* 26 */       if (args[0].equalsIgnoreCase("reload")) {
/* 27 */         Main.pluginReload(sender);
/* 28 */       } else if (args[0].equalsIgnoreCase("help")) {
/* 29 */         sender.sendMessage(new TextComponent("Список команд плагина Lobbys:\n/lobby - телепорт в лобби\n/lobbys reload - перезагрузить плагин\n/tserver %lobby% - телепорт в лобби сервера\n/tserver player %player% %lobby% - телепорт игрока в лобби"));
/*    */       }
/*    */       break;
/*    */     default: 
/* 33 */       sender.sendMessage(new TextComponent("[Lobbys] Используйте /lobbys help для справки"));
/*    */     }
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\commands\CommandLobbys.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */