/*    */ package somepeople.lobbys.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.connection.Server;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.lobbys.data.Lobbys;
/*    */ import somepeople.lobbys.data.Messages;
/*    */ import somepeople.lobbys.main.Main;
/*    */ 
/*    */ public class CommandLobby extends Command
/*    */ {
/*    */   public CommandLobby()
/*    */   {
/* 17 */     super("lobby", "lobbys.lobby", new String[0]);
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 22 */     ProxiedPlayer p = (ProxiedPlayer)sender;
/*    */     
/* 24 */     String target = getTargetTeleport(p.getServer().getInfo().getName());
/*    */     
/* 26 */     if (target.equals("")) {
/* 27 */       p.sendMessage(new TextComponent(Main.msg.teleportNowhere()));
/* 28 */       return;
/*    */     }
/*    */     
/* 31 */     ServerInfo final_server = Main.listLobby.MinOnlineServer(target);
/* 32 */     p.connect(final_server);
/* 33 */     p.sendMessage(new TextComponent(Main.msg.teleportMyself(final_server.getName())));
/*    */   }
/*    */   
/*    */   static String getTargetTeleport(String from) {
/* 37 */     from = from.toLowerCase();
/*    */     
/* 39 */     if (from.startsWith("lobby")) return "";
/* 40 */     if (from.indexOf("lobby") != -1) return "lobby";
/* 41 */     return from;
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\commands\CommandLobby.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */