/*    */ package somepeople.lobbys.commands;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.connection.Server;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import somepeople.lobbys.data.Lobbys;
/*    */ import somepeople.lobbys.data.Messages;
/*    */ import somepeople.lobbys.main.Main;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CommandTServer
/*    */   extends Command
/*    */ {
/*    */   public CommandTServer()
/*    */   {
/* 22 */     super("tserver");
/*    */   }
/*    */   
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 27 */     ProxiedPlayer p = (ProxiedPlayer)sender;
/*    */     
/* 29 */     switch (args.length) {
/*    */     case 1: 
/* 31 */       teleportToLobby(p, args[0]);
/* 32 */       break;
/*    */     case 3: 
/* 34 */       if (args[0].equalsIgnoreCase("player")) { teleportPlayer(p, args[1], args[2]);
/*    */       }
/*    */       break;
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   static void teleportPlayer(ProxiedPlayer sender, String targetPlayer, String targetServer)
/*    */   {
/* 43 */     if (!sender.hasPermission("lobbys.tserver.player")) {
/* 44 */       sender.sendMessage(new TextComponent(Messages.NO_PEX));
/* 45 */       return;
/*    */     }
/*    */     
/* 48 */     ProxiedPlayer player = ProxyServer.getInstance().getPlayer(targetPlayer);
/*    */     
/* 50 */     if (player == null) {
/* 51 */       sender.sendMessage(new TextComponent(Main.msg
/* 52 */         .OfflinePlayer(targetPlayer)));
/*    */       
/* 54 */       return;
/*    */     }
/*    */     
/* 57 */     ServerInfo server = ProxyServer.getInstance().getServerInfo(targetServer);
/*    */     
/* 59 */     if (server == null) {
/* 60 */       sender.sendMessage(new TextComponent(Main.msg
/* 61 */         .OfflineServer(targetServer)));
/*    */       
/* 63 */       return;
/*    */     }
/*    */     
/* 66 */     player.connect(server);
/* 67 */     sender.sendMessage(new TextComponent(Main.msg
/* 68 */       .teleportPlayer(targetPlayer, targetServer)));
/*    */   }
/*    */   
/*    */ 
/*    */   static void teleportToLobby(ProxiedPlayer sender, String target)
/*    */   {
/* 74 */     if (!sender.hasPermission("lobbys.tserver")) {
/* 75 */       sender.sendMessage(new TextComponent(Messages.NO_PEX));
/* 76 */       return;
/*    */     }
/*    */     
/* 79 */     String group = Main.listLobby.findGroup(target);
/*    */     
/* 81 */     if (group.equalsIgnoreCase("")) {
/* 82 */       sender.sendMessage(new TextComponent(Main.msg
/* 83 */         .groupNotFound(target)));
/*    */       
/* 85 */       return;
/*    */     }
/*    */     
/* 88 */     sender.connect(Main.listLobby.MinOnlineServer(group));
/* 89 */     sender.sendMessage(new TextComponent(Main.msg
/* 90 */       .teleportMyself(sender.getServer().getInfo().getName())));
/*    */   }
/*    */ }


/* Location:              C:\Users\trsvd\Documents\minecraft\Lobbys-1.0-SNAPSHOT.jar!\somepeople\lobbys\commands\CommandTServer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */